---
layout: post
title: "About Me"
date: 2016-07-04 00:00:00 +0900
categories: About
tags: About Profile
author: waca
image: /assets/images/background_7.JPG
image2: /assets/images/background_7.JPG
---

## Profile
- **Name** : 박총명
- **Age** : 1993.08.18 (24살)
- **NickName** : waca
- **Mail** : tk5436@naver.com
- **Twitter** : [@waca08_18](https://twitter.com/waca08_18)
- **Facebook** : [tk5436](https://facebook.com/tk5436)
- **GitHub** : [KOR-waCa](https://github.com/kor-waca)
- **Community** : [9XD](https://www.facebook.com/groups/1565641083693087/), [OKKY](http://okky.kr)
- **Hobby**
	* [**Coding**](https://github.com/kor-waca/mean) MEAN Stack 공부
	* **Game**
		- Overwatch `waCa#3485`
		- League of Legend `카왘이`
	* **Music**
		- Fingerstyle Guitar 연주
		- Marunouchi sadistic -시이나 링고
		- Fight -코타로 오시오
		- 황혼 -코타로 오시오
		- 기적의 산 -마사아키 키시베
	* **Travle&Picture**
		- 여행 좋아합니다.
		- 최근 다녀온 곳 : 일본(오사카) (16.03.31 ~ 16.04.03)
	* **못먹는 음식**
		- 생선류..
	* **잘먹는 음식**
		- **고기류!!**
		- [**치킨!!!!**](https://namu.wiki/w/치킨)

## Business Experience
- **한경ITS**(2015.11.02 ~ )
	* 한국경영원 전산법인 한경ITS 로 웹개발자로 일하는 중.
	* 사이트 유지보수 및 신규개발
	* Spring 3.x / mybatis / oracle DB 이용하고 있습니다.

## Education
- **삼성SDS 모범병사 S/W 개발자 양성 과정** 수료 (2015.05.24 ~ 2015.08.14)
	* 웹개발 과정을 공부 했음
- [**MEAN**](https://github.com/kor-waca/mean)
	* **Mongo** : v3.2.6 사용 중
	* **Express** : v4.13.4 사용 중
	* **Angular** : v1.5.3 사용 중
	* **Node** : v6.x 버전과 v4.4.3 버전 사용 중
	* Node 공부 중에 우연히 알게 되어 공부중 (Node 는 16년 4월부터)
	* MEAN 공부는 (2016.06.30 부터)
- **NODE**
	* [**node**](https://github.com/kor-waca/node) : 처음 node 책을보며 express 와 같이 사용하며 개발
	* [**hobbycoding**](https://github.com/kor-waca/hobbyCoding) : 여러 모듈 사용(passport 등) 하여 개발 [**heroku**](https://waca.herokuapp.com) 통하여 배포 (나중에 추가 개발예정)
- **JAVA 8**
	* 그렇게 모던하다 길래 공부 중 입니다.
- [**codewars**](http://www.codewars.com/)
	* Javascript 로 문제 푸는 중
- [**KOR-waCa.github.io**](https://github.com/kor-waca/kor-waca.github.io) : Jekyll 이용 블로그 오픈 ~~이 블로그임~~
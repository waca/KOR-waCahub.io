---
layout: post
title: "MEAN 생성"
date: 2016-07-05 00:00:00 +0900
categories: mean
tags: mean		
---

## 들어가기 전

### 환경
- Window 7 + Eclipse 그리고 OS X(EL Capitan) + Intelli J 사용 중 입니다.
- Node 버전은 v.4.4.3
- Express 는 v4.x
- MongoDB 는 v3.x
- Angular 는 v1.3 사용합니다.
- Eclipse 의 경우 Nodeclipse 플러그인 설치

### 프로젝트 생성
1. [**Meaga Boilerplate**](http://megaboilerplate.com/) 편하게 스타트 프로젝트를 생성할 수 있습니다.
2. 순서대로 **Node.js** -> **Express** -> Bootstrap -> None/CSS -> **AngularJS** -> **Gulp + Browserify** -> **Mocha** -> **MongoDB** -> Email+Facebook+Google+Twitter -> **Heroku** 선택후 **Compile And Download** 클릭 하면 간단하게 프로젝트를 생성할 수 있습니다.
3. **npm install** 로 **module** 생성
4. **package.json** 을 **npm start** 로 실행시킨후 [**127.0.0.1:3000**](http://127.0.0.1:3000)으로 확인
5. ~~직접만들고 싶다면 직접 만들어도 됨~~